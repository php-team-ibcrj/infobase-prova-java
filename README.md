# infobase-prova-java

Reposit�rio para entrega de provas pr�ticas da Infobase utilizando a linguagem Java


# Instru��es

1. Desenvolver um CRUD de cadastro de usu�rios:
	- Nome
	- CPF
	- E-mail
	- Telefone
	
2. Devem existir dois PERFIS
	- USUARIO
		* Edita seu pr�prio perfil, lista usu�rios cadastrados
	- ADMINISTRADOR
		* Possui todas as func��es do USUARIO comum al�m de inserir / editar / excluir usu�rios de ambos os perfis
		
3. Desenvolver as telas necess�rias

4. Os m�todos de lista, inser��o, edi��o e exclus�o tamb�m dever�o ser expostos como servi�os


Recomenda��es:

* O tipo de autentica��o (JWT, OAUTH, etc) do usu�rio � de livre escolha

* Escolha de frameworks fica a cargo do candidato

* O sistema deve ser auto suficiente com instru��es bem expec�ficas de sua execu��o

* O candidato deve criar seu reposit�rio e dar acesso ao usu�rio php-team-ibcrj